# Pi Camera

Pi Camera is a small surveillance App running on a Raspberry PI powered by Flask.

## Inspiration

The main inspiration for this project was to play around with my Raspberry Pi and it's Camera.
So the idea was to build a small web application which is able to stream the video from the connected PI Camera module.

## Screenshots
Stream and Video Controls:
![image](doc/img/stream-settings.png)

Raspberry PI Device Info:
![image](doc/img/device-info.png)
