from flask import Flask
from flask_bootstrap import Bootstrap, StaticCDN
from flask_nav import Nav, register_renderer
from flask_nav.elements import Navbar, View

from config import config

from .nav.renderer import IconViewRenderer, SideMenuRenderer
from .nav.view import IconView
from .utils.tools.system import SystemMonitor

# pylint: disable=import-outside-toplevel

bootstrap = Bootstrap()
nav = Nav()
system_monitor = SystemMonitor(interval=1.0)


def create_sidebar():
    # main Sidebar Navigation
    main_nav = Navbar('Main',
                      IconView('Video Stream',
                               'fas fa-camera',
                               'stream.stream'),
                      IconView('Geräte Info',
                               'fab fa-raspberry-pi',
                               'device.info'))
    return main_nav


def create_app(config_name):
    """
    Factory function to configure and create the application.
    This is especially usefull for loading test configurations

    Args:
        config_name (str): configuration name
    """

    # pylint: disable=unused-argument

    # for details about flask configutarion see
    # https://flask.palletsprojects.com/en/1.1.x/tutorial/factory/

    # create and configure the app
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    # Setup Bootstrap
    bootstrap.init_app(app)
    bootstrap_cdns = app.extensions['bootstrap']['cdns']
    # add local CDN to service local installed components
    # in the apps static folder
    bootstrap_cdns['local'] = StaticCDN()

    # create and register main navigation sidebar
    side_bar = create_sidebar()
    nav.register_element('sidebar', side_bar)
    nav.init_app(app)

    # Note: using an application factory function introduces a problem for
    # routes. In single script applications, the application instance exists in
    # the global scope, so routes can be easilyy defined using the app.route
    # decorator. But now that the application is created at runtime, the
    # app.route decorator begins to exist only after create_app() is invoked,
    # which is too late. Custom error page handler present the same problem,
    # as therse are defined with the app.errorhandler decorator.
    #
    # But luckily Flask offers a better solution, the 'blueprints' ;-)
    # -------------------------------------------------------------------------
    from .stream.routes import stream_bp
    from .device.routes import device_bp
    from .errors.handlers import errors

    # register blueprints
    app.register_blueprint(stream_bp)
    app.register_blueprint(device_bp)
    app.register_blueprint(errors)

    # register renderers
    register_renderer(app, 'sidebar', SideMenuRenderer)
    register_renderer(app, 'iconView', IconViewRenderer)

    # start monitoring thread
    system_monitor.start()

    return app
