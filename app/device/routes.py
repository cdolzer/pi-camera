"""Flask Route for Raspberry Device Information"""
from flask import Blueprint, jsonify, render_template
from ..import system_monitor

device_bp = Blueprint('device', __name__)


@device_bp.route("/info")
def info():
    """
    Device Info View
    """
    return render_template(
        'device.html',
        title='Raspberry Geräte Info',
        sys_info=system_monitor.asdict())


@ device_bp.route("/get-device-info", methods=['GET'])
def get_device_info():
    return jsonify(system_monitor.asdict())
