"""Renderer helper classes"""
from dominate import tags
from flask_nav.renderers import Renderer, SimpleRenderer


class IconViewRenderer(SimpleRenderer):
    """
    A HTML5 renderer supporting Icons for the navigation item

    Args:
        **kwargs: additional arguments to pass onto the root nav.
    """

    def visit_ViewImg(self, node):
        kwargs = {}
        if node.active:
            kwargs['_class'] = 'active'

        view = tags.a(node.text, **kwargs)
        view['href'] = node.get_url()
        view['title'] = node.text

        icon = view.add(tags.i())
        icon['class'] = 'nav-icon {}'.format(node.icon)

        return view


class SideMenuRenderer(Renderer):
    """
    A HTML5 renderer for a sidebar navigation
    """

    def visit_Navbar(self, node):
        root = tags.div()
        root['class'] = 'sidebar'

        cont = root.add(tags.ul())
        cont['class'] = 'nav nav-pills nav-sidebar flex-column'
        cont['data-widget'] = 'treeview'
        cont['role'] = 'menu'
        cont['data-accordion'] = 'false'

        for item in node.items:
            cont.add(self.visit(item))

        return root

    def visit_Text(self, node):
        return tags.p(node.text)

    def visit_Link(self, node):
        item = tags.li()
        item.add(tags.a(node.text, href=node.get_url()))

    def visit_IconView(self, node):
        item = tags.li(cls='nav-item')
        link = item.add(tags.a(href=node.get_url()))

        if node.active:
            link['class'] = 'nav-link active'
        else:
            link['class'] = 'nav-link'

        link.add(
            tags.i(cls='nav-icon {}'.format(node.icon)))
        link.add(tags.p(node.text))

        return item
