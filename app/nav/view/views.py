""" Extensions for Flask-Nav Elements"""
from flask_nav.elements import View


class IconView(View):
    def __init__(self, text, icon, endpoint, *args, **kwargs):
        self.icon = icon

        super().__init__(text, endpoint, *args, **kwargs)
