"""Video Stream Route"""

from flask import Blueprint, Response, render_template, request, url_for
from ..utils.video import VideoStream

stream_bp = Blueprint('stream', __name__)
video = VideoStream(orientation=180, framerate=30)

# pylint: disable=global-statement

# Exposure Modes
exposure_modes = [
    ('off', 'Aus'),
    ('auto', 'Auto'),
    ('night', 'Nacht'),
    ('backlight', 'Hintergrundbeleuchtung'),
    ('spotlight', 'Scheinwerfer'),
    ('snow', 'Schnee'),
    ('beach', 'Strand'),
    ('verylong', 'Sehr Lange'),
    ('fireworks', 'Feuerwerk')]

white_balance = [
    ('off', 'Aus'),
    ('auto', 'Auto'),
    ('sunlight', 'Sonnenlicht'),
    ('cloudy', 'Bewölkt'),
    ('shade', 'Schattig'),
    ('tungsten', 'Kunstlicht'),
    ('fluorescent', 'Neonlicht'),
    ('incandescent', 'Glühlampe'),
    ('horizon', 'Horizont')]


@stream_bp.route("/", methods=['GET', 'POST'])
@stream_bp.route("/stream", methods=['GET', 'POST'])
def stream():
    global video, exposure_modes, white_balance

    if request.method == 'POST':
        video.exposure_mode = request.form.get('exposure')
        video.white_balance = request.form.get('whiteness')

    if video.is_streaming():
        stream_state = True
        video_url = url_for("stream.video_feed")
    else:
        stream_state = False
        video_url = url_for("static", filename="/img/nostream.png")

    selected_mode = video.exposure_mode
    selected_white_balance = video.white_balance

    return render_template(
        'stream.html',
        title='Video Stream',
        stream_state=stream_state,
        video_url=video_url,
        exposure_modes=exposure_modes,
        selected_mode=selected_mode,
        white_balance=white_balance,
        selected_white_balance=selected_white_balance)


@ stream_bp.route("/video_feed")
def video_feed():
    global video
    # return the response generated along with the specific media
    # type (mime type)
    # return Response(generate(),
    return Response(video.capture(),
                    mimetype="multipart/x-mixed-replace; boundary=frame")


@ stream_bp.route("/toggle-stream", methods=['GET', 'POST'])
def toggle():
    global video

    response = Response(
        url_for("static", filename="/img/nostream.png"))

    if request.method == 'POST':
        data = request.form.get("state")
        if data == 'true':
            video.start()
            response = Response(url_for("stream.video_feed"))
        else:
            video.stop()

    return response
