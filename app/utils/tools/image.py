""" Helper tools for image (frame) handling"""

import cv2


def rotate(image, angle, center=None, scale=1.0):
    """Rotates an image (frame)

    Args:
        image  : Source Image
        angle  : Rotation angle in degrees. Positive values mean
                 counter-clockwise rotation
                 (the coordinate origin is assumed to be the top-left corner).
        center : [optional] Center of the rotation in the source image.
        scale  : [optional] Scale factor. Defaults to 1.0.

    Returns:
        [type]: [description]
    """
    # grab the dimensions of the image
    (img_h, img_w) = image.shape[:2]

    # if the center is None, initialize it as the center of
    # the image
    if center is None:
        center = (img_w // 2, img_h // 2)

    # perform the rotation
    matrix = cv2.getRotationMatrix2D(center, angle, scale)
    rotated = cv2.warpAffine(image, matrix, (img_w, img_h))

    # return the rotated image
    return rotated


def resize(image, width=None, height=None, interpolation=cv2.INTER_AREA):
    """Resize an image

    Args:
        image  : source image
        width  : image width. Defaults to None.
        height : image height. Defaults to None.
        interpolation: (int, optional) interpolation mode. 
    """
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (img_h, img_w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        ratio = height / float(img_h)
        dim = (int(img_w * ratio), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        ratio = width / float(img_w)
        dim = (width, int(img_h * ratio))

    # resize the image
    resized = cv2.resize(image, dim, interpolation=interpolation)

    # return the resized image
    return resized


def adjust_brightness_contrast(image, brightness=0., contrast=0.):
    """Adjust the brightness and/or contrast of an image

    Args:
        image     : OpenCV BGR image
        brightness: (float, optional): contrast adjustment with 0 meaning no change
        contrast  : ([type], optional): brightness adjustment with 0 meaning no change
    """

    beta = 0
    # See the OpenCV docs for more info on the `beta` parameter to addWeighted
    # https://docs.opencv.org/3.4.2/d2/de8/group__core__array.html#gafafb2513349db3bcff51f54ee5592a19
    return cv2.addWeighted(image,
                           1 + float(contrast) / 100.,
                           image,
                           beta,
                           float(brightness))
