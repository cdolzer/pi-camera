"""
the system module is a library for retrieving information about the PI's
system utilization (CPU, memory, disk, ...)
"""
import subprocess
import time
from dataclasses import dataclass
from threading import Lock, Thread

import psutil


def _get_cpu_info():
    """Get information from the PI's CPU

    Information read:
        - 'count' : nr of cpu's
        - 'min_freq' : minimum cpu frequency
        - 'max_freq' : maximum cpu frequency
    """
    cpu_freq = psutil.cpu_freq()

    return CpuInfo(
        psutil.cpu_count(),
        cpu_freq.min,
        cpu_freq.max)


def _get_device_info():
    """Gets basic system information from '/proc/meminfo'

    Reads the Pi's 'Hardware', 'Revision', 'Model', 'Serial' informtion
    Returns:
        [dict]: dictionary with memory information
    """

    # filter keys which line we want to read from the file
    keys = ['Hardware', 'Revision', 'Model', 'Serial']

    with open('/proc/cpuinfo') as f:
        lines = f.read().splitlines()

    device = {x[0].strip(): x[1].strip() for x in [
        item.split(':') for item in lines if item.startswith(tuple(keys))]}

    return DeviceInfo(
        device['Hardware'],
        device['Revision'],
        device['Model'],
        device['Serial'])


def _get_pi_cpu_sensor():
    """Get the PI's CPU Temperature"""

    proc = subprocess.run(["/opt/vc/bin/vcgencmd", "measure_temp"],
                          capture_output=True, text=True, check=True)

    if proc.returncode == 0:
        # no errors so process the output
        temperature_error = 'No error'  # use this as you will in the call
        temperature = proc.stdout[5:-3]   # remove the surrounding text
    else:
        # there was an error
        temperature_error = proc.stderr

    return temperature, temperature_error


def _get_mem_info():
    """Gets memory information from '/proc/meminfo'

    Read the current available memory information in MB
    """
    keys = ['MemTotal', 'MemFree']

    with open('/proc/meminfo') as f:
        lines = f.read().splitlines()

    # filter items for given key's
    items = [item.split(':') for item in lines if item.startswith(tuple(keys))]

    # convert into dictonary and format memory into MB (Mibi)
    info = {x[0].strip(): float(int((x[1].strip().split(' '))[0]) / 1024.0)
            for x in items}

    return MemInfo(info['MemTotal'], info['MemFree'])


def _get_disk_info():
    """Get information about the PI's file system

    Information read:
        - 'total' : total disk space in MB
        - 'free'  : free disk space in MB
        - 'perc'  : free disk space in %

    Returns:
        [type]: [description]
    """
    factor = 1 / 1024 / 1024
    disk = psutil.disk_usage('/')

    return MemInfo(disk.total * factor, disk.free * factor)


def asdict(f):
    """ Converts Class properties into a Dictonary

    Other than the asdict() function from 'dataclasses' package will this
    version also consider properties.

    Class Attributes starting with a '_' will be ignored

    """
    ret = {prop: getattr(f, prop)
           for prop in dir(f) if not prop.startswith('_')}
    return ret


@dataclass
class CpuInfo:
    num: int
    min_freq: int
    max_freq: int
    temp: int
    load: float

    def __init__(self, num, min_freq, max_freq):
        self.num = num
        self.min_freq = min_freq
        self.max_freq = max_freq
        self.temp = _get_pi_cpu_sensor()
        self.load = psutil.cpu_percent()


@dataclass
class MemInfo:
    total: float
    free: float

    def __init__(self, total, free):
        self.total = total
        self.free = free

    @property
    def used(self):
        return self.total - self.free

    @used.setter
    def used(self, v):
        raise AttributeError('used is not a writable attribute')

    @property
    def perc(self):
        return round((self.total - self.free) * 100 / self.total, 2)

    @perc.setter
    def perc(self, v):
        raise AttributeError('perc is not a writable attribute')


@dataclass
class DeviceInfo:
    hwid: str
    revision: str
    model: str
    serial: str

    def __init__(self, hwid, rev, model, serial):
        self.hwid = hwid
        self.revision = rev
        self.model = model
        self.serial = serial


class SystemMonitor:
    def __init__(self, interval=1.0):
        self.interval = interval
        self.device = _get_device_info()
        self.cpu = _get_cpu_info()
        self.cpu.temp, _ = _get_pi_cpu_sensor()
        self.mem = _get_mem_info()
        self.disk = _get_disk_info()

        # Thread lock object
        self._stopped = False
        self._lock = Lock()
        self._thread = Thread(target=self._update,
                              name="DeviceMonitor", args=())
        self._thread.daemon = True

    def start(self):
        self._thread.start()

    def _update(self):
        while not self._stopped:
            with self._lock:  # do we need that
                self.cpu.temp, _ = _get_pi_cpu_sensor()
                self.cpu.load = psutil.cpu_percent()
                self.mem = _get_mem_info()
                self.disk = _get_disk_info()
            time.sleep(self.interval)
        self._stopped = False

    def asdict(self):
        return {
            "device": asdict(self.device),
            "cpu": asdict(self.cpu),
            "mem": asdict(self.mem),
            "disk": asdict(self.disk)
        }
