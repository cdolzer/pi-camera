"""Video Stream Module for PI Camera"""
from threading import Event, Thread
from picamera import PiCamera

from picamera.array import PiRGBArray


class PiVideoStream:
    """[summary]
    """

    def __init__(self, resolution=(320, 240), framerate=32, **kwargs):
        """Initialize the PI Camera Stream

        Args:
            resolution (tuple, optional): [description]. Defaults to (320, 240).
            framerate (int, optional): [description]. Defaults to 32.
        """

        # initialize the camera
        self.camera = PiCamera()
        self.event = Event()

        # set camera parameters
        self.camera.resolution = resolution
        self.camera.framerate = framerate
        self.camera.awb_mode = 'tungsten'
        self.camera.exposure_mode = 'backlight'

        # set optional camera parameters (refer to PiCamera docs)
        for (arg, value) in kwargs.items():
            setattr(self.camera, arg, value)

        # initialize the stream
        self.raw_capture = PiRGBArray(self.camera, size=resolution)
        self.stream = self.camera.capture_continuous(
            self.raw_capture,
            format="bgr",
            use_video_port=True)

        # initialize the frame and the variable used to indicate
        # if the thread should be stopped
        self.frame = None
        self.stopped = False

    def start(self):
        """Start the video stream

            Start the thread to read frames from the video stream
        """
        # start the thread to read frames from the video stream
        self.stopped = False
        th = Thread(target=self._update, name="PiVideoStream", args=())
        th.daemon = True
        th.start()
        return self

    def _update(self):
        """Camera background thread"""

        # keep looping infinitely until the thread is stopped
        for f in self.stream:
            # grab the frame from the stream and clear the stream in
            # preparation for the next frame
            self.frame = f.array
            self.raw_capture.truncate(0)

            if self.event is not None:
                self.event.set()    # Set Event

            # if the thread indicator variable is set, stop the thread
            # and resource camera resources
            if self.stopped:
                self.stream.close()
                self.raw_capture.close()
                self.camera.close()
                return

    def read(self):
        # return the frame most recently read
        return self.frame

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True

    def get_event(self):
        return self.event
