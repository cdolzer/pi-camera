"""Video Stream"""
import time
from threading import Lock, Thread

import cv2
from ..tools import image
from ..video.pivideostream import PiVideoStream


class VideoStream:
    """Video Stream Class"""

    def __init__(self, resolution=(640, 480),
                 orientation=0.0,
                 framerate=25, **kwargs):
        self._output_frame = None
        self._framerate = framerate

        # initialize the picamera stream and allow the camera
        # sensor to warmup
        self.stream = PiVideoStream(
            resolution=resolution,
            framerate=framerate,
            **kwargs)
        # start the stream thread (daemon) of the PiCamera
        self.event = self.stream.event
        self.stream.start()

        # wait until frames are available
        while self.stream.read() is None:
            time.sleep(0.5)

        # Thread Lock object
        self._lock = Lock()
        self._streaming = False
        self._orientation = orientation

    # Propperty
    @property
    def exposure_mode(self):
        """get the current video exposure mode"""
        return self.stream.camera.exposure_mode

    @exposure_mode.setter
    def exposure_mode(self, value):
        """sets the video exposure mode

        Args:
            value (str): Exposure mode to be set
        """
        if value in self.stream.camera.EXPOSURE_MODES.keys():
            self.stream.camera.exposure_mode = value

    @property
    def white_balance(self):
        """get the current white balance mode"""
        return self.stream.camera.awb_mode

    @white_balance.setter
    def white_balance(self, value):
        """sets the video white balance mode

        Args:
            value (str): white balance mode to be set
        """
        if value in self.stream.camera.AWB_MODES.keys():
            self.stream.camera.awb_mode = value

    # Frame Capture Thread
    def _capture_thread(self, event):
        """image capture thread
        """
        while self._streaming:
            if event is not None:
                event.wait()    # wait for the next frame
                event.clear()   # reset the event flag

            frame = self.stream.read()

            if frame is not None:
                # frame = resize(frame, width=640)
                if self._orientation != 0:
                    frame = image.rotate(frame, self._orientation)

                # acquire the lock, set the output frame, and relase the lock
                with self._lock:
                    self._output_frame = frame.copy()

    def start(self):
        """Start the threaded capture stream
        """

        if not self._streaming:
            # start the capture thread
            th = Thread(target=self._capture_thread,
                        name="CaptureStream",
                        args=(self.event,))
            th.daemon = True
            self._streaming = True
            th.start()

    def stop(self):
        """Stop the thread and release any resources
        """
        self._streaming = False

    def is_streaming(self):
        """get streaming status

        Returns:
            bool: True if streaming, otherwise False
        """
        return self._streaming

    def capture(self):
        """Generator used to ecode the captured frame into JPEG
        """

        if not self._streaming:
            yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
                  bytearray() + b'\r\n')

        # loop over frames from the output stream
        while self._streaming:
            # wait until lock is acquired
            with self._lock:
                # check if the output frame is available, otherwise skip
                # the iteration of the loop
                if self._output_frame is None:
                    continue

                # encode the frame in JPEG format
                (flag, encoded_image) = cv2.imencode(
                    ".jpg", self._output_frame)

                # ensure the frame was sucessfully encoded
                if not flag:
                    continue

            # yield the output frame in the byte format so that it
            # can be consumed by a webbrowser
            yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
                  bytearray(encoded_image) + b'\r\n')
