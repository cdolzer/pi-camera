""" The Config Base Class contains settings that are common to all configurations.

The different subclasses define settings that are specific to a configuration.
Additional configuration can be added as needed."""

import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get(
        'SECRET_KEY') or 'hard to guess fallback string'
    BOOTSTRAP_SERVE_LOCAL = True
    BOOTSTRAP_QUERYSTRING_REVVING = True  # is this needed?

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    BOOTSTRAP_USE_MINIFIED = True


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    pass


# register the different configurations in a dictionary
config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
