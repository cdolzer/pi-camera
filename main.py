"""The Pi Camera Flask Web App"""
import os
#import click

from app import create_app

# pylint: disable=import-outside-toplevel

app = create_app(os.getenv('FLASK_CONFIG') or 'default')

# @app.shell_context_processor
# def make_shell_context():
#     return dict(db=db, User=User, Role=Role)




if __name__ == "__main__":
    # start the flask app

    app.run(host='0.0.0.0',
            threaded=True,
            debug=False,
            use_reloader=False)
